### Hackathon June -18
Documentation of a few projects hacked together in June 2018.
##### Timecon punching automator
**Problem**  
Employees are forgetting to use Timecon and have to insert all working hours day by day for a longer period of time.  
**Solution**  
Chrome bookmarklets to make this process faster.  
**Demo**: http://recordit.co/1UbzIYg8yJ
#

##### Employee well-being monitor
**Problem**  
The HR department wants to have a better understanding on the employee well-being and fluctuation during the year.  
**Solution**  
A tablet web application, (placed e.g. next to the coffee machine), that stores data about how people are doing.  
**Demo**: http://g.recordit.co/6Hh9s7G2R5.gif
