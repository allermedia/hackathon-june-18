/* script.js */

//Execute when document has loaded
document.addEventListener("DOMContentLoaded", function(event) {
  //Set headline through JS ;)
  document.getElementsByTagName('h1')[0].innerHTML = document.title;
  //Construct 10 number buttons
  for (var i = 1; i < 11; i++) {
    document.getElementById('numberContainer').innerHTML += '<div class="number"><span>'+[i]+'</span></div>';
  }
});

//Fade animation on load
window.onload = function() {
  document.body.className += 'fade-in';
};

//Click event to numbers
setTimeout(function() {
  for (var j = 0; j < 10; j++) {
    document.getElementsByClassName('number')[j].addEventListener("click", function(event) {
      document.getElementById('thanks').innerHTML = "Thanks for your submission :)";
      this.style = "background: #fff; color: #000; font-weight: bold; border-color: #d40f18;";
      setTimeout(function(){
        //Refresh page after 3 secs
        document.location.href = document.location.href;
      }, 3000);
    });
  }
}, 1500);
